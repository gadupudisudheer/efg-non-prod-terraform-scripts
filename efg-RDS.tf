data "aws_subnet_ids" "all" {
	vpc_id = var.vpc_id
}

resource "aws_db_subnet_group" "rds-subnet" {
	name  = "efg-ac-db-subnets"
	subnet_ids = data.aws_subnet_ids.all.ids

     tags        = {
          "ApplicationAcronym"   = "SharedApp"
          "ApplicationCI"        = "SharedApp"
          "ApplicationOwner"     = "Middleware"
          "ApplicationSupport"   = "Middleware"
          "Backup"               = "False"
          "BusinessOwner"        = "Bpost"
          "BusinessUnit"         = "Middleware"
          "CloudServiceProvider" = "AWS"
          "CompanyName"          = "TCS"
          "CostCentre"           = "Bpost"
          "Environment"          = "Non Prod"
          "Owner"                = "LST_ICT_DL_MIDDLEWARE@bpost.be"
          "PrimaryContact"       = "LST_ICT_DL_MIDDLEWARE@bpost.be"
          "Project"              = "Middleware"
          "ResourceType"         = "RDS"
          "Tier"                 = ""
        }
}
 
resource "aws_db_instance" "EFG-AC-DB" {
	allocated_storage  				    = 350
	max_allocated_storage               = 1500
	storage_type 						= "gp2"
	engine 		 						= "oracle-se2"
	engine_version 						= "12.2.0.1.ru-2020-01.rur-2020-01.r1"
	instance_class						= "db.m5.xlarge"
	db_subnet_group_name				= aws_db_subnet_group.rds-subnet.id
	storage_encrypted					= "true"
	license_model						= "license-included"
	name								= "EFGACDB"
	username							= "admin"
	#password							= "password2020"
	performance_insights_enabled        = true
	port 								= "1523"
	iam_database_authentication_enabled = "false"
	vpc_security_group_ids				= ["sg-04ed814ae88399779"]
	maintenance_window					= "sun:01:30-sun:02:00"
	backup_window						= "00:30-01:00"
	backup_retention_period				= 1
	#final_snapshot_identifier			= "EFG-AC-Final-Snapshot"
	character_set_name					= "AL32UTF8"
	deletion_protection					= "true"
	auto_minor_version_upgrade			= "false"
	copy_tags_to_snapshot				= "true"
	skip_final_snapshot                 = true
	kms_key_id							= "arn:aws:kms:eu-west-1:528181836254:key/d7c4a36b-5791-4bca-bdf9-b55ccce962ff"
	
tags	= {
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "AvailabilityZone"     = "eu-west-1c"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "AC"
    "ManagedBy"            = "Infosys"
    "Name"                 = "rdi-mw-ac-euwe01-efg-01"
    "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
    "PatchGroup"           = "RHEL"
    "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "Project"              = "EFG"
    "ResourceType"         = "RDS"
    "Tier"                 = "App"
	"UseCase"              = "3"
   }
}

resource "aws_sns_topic" "default" {
	name 	= "EFG-AC-DB-Events"
tags 		= {
	
	Name = "sns-sharedapp-np-euwe01-efg-01"
	ApplicationAcronym = "EFG"
	ApplicationCI = "EFG"
	ApplicationOwner = "Ramesh.Balachandran.ext@bpost.be"
	ApplicationSupport = "Ramesh.Balachandran.ext@bpost.be"
	Backup = "False"
	BusinessOwner = "Bpost"
	BusinessUnit = "Middleware"
	CloudServiceProvider = "AWS"
	CompanyName = "Infosys"
	CostCentre = "Bpost"
	DataProfile = "Internal"
	DRLevel = "1"
	Environment = "AC"
	Owner = "Ramesh"
	PatchGroup = "Oracle"
	Project = "EFG"
	ResourceType = "SNS"
	Tier = "App"
	"UseCase"     = "3"
	}	
}

resource "aws_db_event_subscription" "default" {
	name 		= "EFG-AC-DB-Events-Subs"
	sns_topic   = aws_sns_topic.default.arn
	source_type = "db-instance"
	source_ids  = [aws_db_instance.EFG-AC-DB.id]
	
	event_categories = [
		"availability",
		"deletion",
		"failover",
		"low storage",
		"maintenance",
		"notification",
		"read replica",
		"recovery",
		"restoration",
	]

}

