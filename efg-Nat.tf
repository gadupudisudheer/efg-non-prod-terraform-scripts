resource "aws_nat_gateway" "EFG-Nat" {
	allocation_id = "eipalloc-065f1231b8e3802e7"
	subnet_id	  = var.public_subnet_id
	
tags                         = {
    Name = "ngw-sharedapp-np-euwe01-001"
	ApplicationAcronym = "EFG"
	ApplicationCI = "EFG"
	ApplicationOwner = "Ramesh.Balachandran.ext@bpost.be"
	ApplicationSupport = "Ramesh.Balachandran.ext@bpost.be"
	Backup = "False"
	BusinessOwner = "Bpost"
	BusinessUnit = "Middleware"
	CloudServiceProvider = "AWS"
	CompanyName = "Infosys"
	CostCentre = "Bpost"
	DataProfile = "Internal"
	DRLevel = "1"
	Environment = "AC"
	Owner = "Ramesh"
	PatchGroup = "RHEL"
	Project = "EFG"
	ResourceType = "Nat Gateway"
	Tier = "App"
	}
}