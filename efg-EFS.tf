resource "aws_efs_file_system" "efg-ac" {
	creation_token = "EFG-EFS-AC"
	performance_mode = "generalPurpose"
	throughput_mode = "bursting"
	encrypted = "true"
	
	lifecycle_policy {
		transition_to_ia = "AFTER_30_DAYS"
	}
	
	tags   = {
        "ApplicationAcronym"   = "EFG"
        "ApplicationCI"        = "EFG"
        "ApplicationName"      = "Enterprise File Gateway" 
        "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
        "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
        "Backup"               = "False"
        "BusinessOwner"        = "Bpost"
        "BusinessUnit"         = "Middleware"
        "CloudServiceProvider" = "AWS"
        "CompanyName"          = "Infosys"
        "CostCentre"           = "Bpost"
        "DRLevel"              = "1"
        "DataProfile"          = "Internal"
        "Environment"          = "AC"
        "ManagedBy"            = "Infosys" 
        "Name"                 = "efs-sharedapp-np-euwe01-002"
        "Owner"                = "Ramesh"
        "PatchGroup"           = "RHEL"
        "Project"              = "EFG"
        "ResourceType"         = "EFS FileShare"
        "Tier"                 = "App"
		"UseCase"              = "3"
    }
}

resource "aws_efs_mount_target" "EFG-mount-target" {
	count = 3
	file_system_id  = aws_efs_file_system.efg-ac.id
	subnet_id       = element(var.EFS_subnet_id, count.index)
	security_groups = ["sg-0d312c0b4c5bd9270"]
	
}
	