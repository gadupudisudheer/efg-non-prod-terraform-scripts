# EFG - CORE Server in private subnet
###########################################################################################################


resource "aws_instance" "EFG-core" {
  count						   = var.ec2-count
  ami                          = var.ec2-ami # Use the latest version of the Golden image and please do not use Marketplace images
  instance_type                = var.core-instance-type
    #associate_public_ip_address = "true" # Set to true if public ip address needs to be attached to the instance
  subnet_id                    = var.subnet_id
  iam_instance_profile         = "iar-SsmAdmin"
  key_name 				       = "key-sharedapp-np-euwe01-01"
  vpc_security_group_ids       = ["sg-0c214031fb11b1573","sg-073c00a3f1734fe62","sg-0cdedc97251fa7d40"] # ID's of Security group you want to attach with the instance
  disable_api_termination      = "false" # Deletion protection
  #depends_on                   = ["aws_ebs_encryption_by_default.ebs_def","aws_ebs_default_kms_key.key_def"]
  
   ebs_optimized                = true
  
  tags  = {
         "ApplicationAcronym"   = "EFG"
         "ApplicationCI"        = "EFG"
         "ApplicationName"      = "Enterprise File Gateway" 
         "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
         "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
         "Automation"           = "Yes" 
         "AvailabilityZone"     = "eu-west-1" 
         "Backup"               = "False"
         "BusinessOwner"        = "Bpost"
         "BusinessUnit"         = "Middleware"
         "CloudServiceProvider" = "AWS"
         "CompanyName"          = "Infosys"
         "CostCentre"           = "Bpost"
         "DRLevel"              = "1"
         "DataProfile"          = "Internal"
         "Environment"          = "AC"
         "ImageVersionName"     = "image-rhel7-we-v04" 
         "ManagedBy"            = "Infosys" 
         "Name"                 = "PLNP05001"
         "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
         "PatchGroup"           = "RHEL"
         "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be" 
         "Project"              = "EFG"
         "ResourceType"         = "EC2"
         "Tier"                 = "App"
		 "UseCase"              = "3"
    }
	volume_tags	= {
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationName"      = "Enterprise File Gateway"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "Automation"           = "Yes"
    "AvailabilityZone"     = "eu-west-1"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "AC"
    "ImageVersionName"     = "image-rhel7-we-v04"
    "ManagedBy"            = "Infosys"
    "Name"                 = "PLNP05001"
    "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
    "PatchGroup"           = "RHEL"
    "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "Project"              = "EFG"
    "ResourceType"         = "EC2"
    "Tier"                 = "App"
	"UseCase"              = "3"
    }
	root_block_device	{
		volume_size = "75"
    }

}

resource "aws_ebs_volume" "EFG-Core-sec-drive" {
availability_zone = "eu-west-1b"
size = 400
type = "gp2"
encrypted  = "true"
kms_key_id = var.kms_key

tags  = {
         "ApplicationAcronym"   = "EFG"
         "ApplicationCI"        = "EFG"
         "ApplicationName"      = "Enterprise File Gateway"
         "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
         "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
         "Automation"           = "Yes"
         "AvailabilityZone"     = "eu-west-1"
         "Backup"               = "False"
         "BusinessOwner"        = "Bpost"
         "BusinessUnit"         = "Middleware"
         "CloudServiceProvider" = "AWS"
         "CompanyName"          = "Infosys"
         "CostCentre"           = "Bpost"
         "DRLevel"              = "1"
         "DataProfile"          = "Internal"
         "Environment"          = "AC"
         "ImageVersionName"     = "image-rhel7-we-v04"
         "ManagedBy"            = "Infosys"
         "Name"                 = "PLNP05001"
         "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
         "PatchGroup"           = "RHEL"
         "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
         "Project"              = "EFG"
         "ResourceType"         = "EC2"
         "Tier"                 = "App"
		 "UseCase"              = "3"
        }
}

resource "aws_volume_attachment" "EFG-Core-sec-drive" {
device_name = "/dev/sdb"
volume_id = aws_ebs_volume.EFG-Core-sec-drive.id
instance_id = aws_instance.EFG-core[0].id
}

# Perimeter Server 1 in private subnet
###########################################################################################################

resource "aws_instance" "EFG-P1" {
  count						   = var.ec2-count
  ami                          = var.ec2-ami # Use the latest version of the Golden image and please do not use Marketplace images
  instance_type                = var.instance-type
    #associate_public_ip_address = "true" # Set to true if public ip address needs to be attached to the instance
  subnet_id                    = var.subnet_id
  iam_instance_profile         = "iar-SsmAdmin"
  key_name 				       = "key-sharedapp-np-euwe01-01"
  vpc_security_group_ids       = ["sg-0c214031fb11b1573","sg-073c00a3f1734fe62","sg-0cdedc97251fa7d40"] # ID's of Security group you want to attach with the instance
  disable_api_termination      = "false" # Deletion protection
  #depends_on                   = ["aws_ebs_encryption_by_default.ebs_def","aws_ebs_default_kms_key.key_def"]
  
  tags  = {
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationName"      = "Enterprise File Gateway"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "Automation"           = "Yes"
    "AvailabilityZone"     = "eu-west-1"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "AC"
    "ImageVersionName"     = "image-rhel7-we-v04"
    "ManagedBy"            = "Infosys"
    "Name"                 = "PLNP05002"
    "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
    "PatchGroup"           = "RHEL"
    "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "Project"              = "EFG"
    "ResourceType"         = "EC2"
    "Tier"                 = "App"
	"UseCase"              = "3"
    }
	volume_tags            = {
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationName"      = "Enterprise File Gateway"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "Automation"           = "Yes"
    "AvailabilityZone"     = "eu-west-1"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "AC"
    "ImageVersionName"     = "image-rhel7-we-v04"
    "ManagedBy"            = "Infosys"
    "Name"                 = "PLNP05002"
    "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
    "PatchGroup"           = "RHEL"
    "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "Project"              = "EFG"
    "ResourceType"         = "EC2"
    "Tier"                 = "App"
	"UseCase"              = "3"
    }
	
	root_block_device	{
	    delete_on_termination = false
		volume_size = "100"
    }

}

resource "aws_ebs_volume" "EFG-P1-sec-drive" {
availability_zone = "eu-west-1b"
size = 70
type = "gp2"
encrypted  = "true"
kms_key_id = var.kms_key

tags   = {
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationName"      = "Enterprise File Gateway"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "Automation"           = "Yes"
    "AvailabilityZone"     = "eu-west-1"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "AC"
    "ImageVersionName"     = "image-rhel7-we-v04"
    "ManagedBy"            = "Infosys"
    "Name"                 = "PLNP05002"
    "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
    "PatchGroup"           = "RHEL"
    "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "Project"              = "EFG"
    "ResourceType"         = "EC2"
    "Tier"                 = "App"
	"UseCase"              = "3"
   }
}

resource "aws_volume_attachment" "EFG-P1-sec-drive" {
device_name = "/dev/sdb"
volume_id = aws_ebs_volume.EFG-P1-sec-drive.id
instance_id = aws_instance.EFG-P1[0].id
}

# Perimeter Server 2 in public subnet
###########################################################################################################

resource "aws_instance" "EFG-P2" {
  count						   = var.ec2-count
  ami                          = "ami-012551911e32efaf4"#var.ec2-ami # Use the latest version of the Golden image and please do not use Marketplace images
  instance_type                = var.instance-type
  associate_public_ip_address  = "true" # Set to true if public ip address needs to be attached to the instance
  private_ip				   = "10.76.192.228"#"10.76.195.115"
  subnet_id                    = var.public_subnet_id
  iam_instance_profile         = "iar-SsmAdmin"
  key_name 				       = "key-sharedapp-np-euwe01-01"
  ebs_optimized                = true
  hibernation                  = false
  monitoring                   = false
  vpc_security_group_ids       = ["sg-0c214031fb11b1573","sg-04006d0a9137df3cb","sg-073c00a3f1734fe62","sg-0cdedc97251fa7d40"] # ID's of Security group you want to attach with the instance
  disable_api_termination      = true # Deletion protection
 # depends_on                   = ["aws_ebs_encryption_by_default.ebs_def","aws_ebs_default_kms_key.key_def"]
  
   tags  = {
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationName"      = "Enterprise File Gateway"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "AvailabilityZone"     = "eu-west-1"
	"Automation"           = "Yes"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "Prod"#"AC"
    "ImageVersionName"     = ""#"image-rhel7-we-v04"
    "ManagedBy"            = "Infosys"
    "Name"                 = "PLNP05003"#"PLNP05003-DMZ"
    "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
	"PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "PatchGroup"           = "RHEL"
    "Project"              = "EFG"
    "ResourceType"         = "EC2"
    "Tier"                 = "App"
	"UseCase"              = "3"
    }
	volume_tags            = {
    "Application"          = "EFG"
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationName"      = "Enterprise File Gateway"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "Automation"           = "Yes"
    "AvailabilityZone"     = "eu-west-1"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "Prod"
    "ImageVersionName"     = ""
    "ManagedBy"            = "Infosys"
    "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
    "PatchGroup"           = "RHEL"
    "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "Project"              = "EFG"
    "ResourceType"         = "EC2"
    "Tier"                 = "App"
	"UseCase"              = "3"
    }
	root_block_device	{
	    delete_on_termination = false
		volume_size = "100"
}

}

resource "aws_ebs_volume" "EFG-P2-sec-drive" {
availability_zone = "eu-west-1b"
size = 70
type = "gp2"
encrypted  = "true"
kms_key_id = var.kms_key

tags  = {
    "Application"          = "EFG"
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationName"      = "Enterprise File Gateway"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "AvailabilityZone"     = "eu-west-1"
	"Automation"		   = "Yes"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "Prod"#"AC"
    "ImageVersionName"     = ""#"image-rhel7-we-v04"
    "ManagedBy"            = "Infosys"
    #"Name"                 = "PLNP05003"
    "Owner"                = "Ramesh.Balachandran.ext@bpost.be"
	"PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "PatchGroup"           = "RHEL"
    "Project"              = "EFG"
    "ResourceType"         = "EC2"
    "Tier"                 = "App"
	"UseCase"              = "3"
    }
}

resource "aws_volume_attachment" "EFG-P2-sec-drive" {
device_name = "/dev/sdb"
volume_id = aws_ebs_volume.EFG-P2-sec-drive.id
instance_id = aws_instance.EFG-P2[0].id
}

resource "aws_eip_association" "EIP-P2" {
	instance_id = aws_instance.EFG-P2[0].id
	allocation_id = "eipalloc-0dc0690f99e2761a5"
	#allow_reassociation = true
}

