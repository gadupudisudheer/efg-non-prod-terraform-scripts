variable "region"{
	default = "eu-west-1"
}

variable "vpc_cidr" {
	default = "10.76.192.0/19"	
}

variable "vpc_id" {
	default = "vpc-03d09439393ffa532"
}	

variable "subnet_cidr" {
		default = "10.76.200.0/23"
}

variable "public_subnet_cidr" {
		default = "10.76.194.0/24"
}

variable "subnet_id" {
	default = "subnet-053456aed521f6b98"
}

variable "EFS_subnet_id" {
type = list
default = ["subnet-053456aed521f6b98","subnet-06da6b433110e35a8","subnet-0322cb477fe4eca02"]
}

variable "public_subnet_id" {
	default = "subnet-0fced7dd5262598b0"#"subnet-05a7b07a436335c19"
}

variable "azs" {
 	default = "eu-west-1b"
}

variable "ec2-ami" {
  default = "ami-052daf77212bbdf42"
   
}

variable "alarms_email" {
 	default = "ramesh.balachandran.ext@bpost.be"
}

variable "instance-type"{
	default = "t3.medium"
}

variable "core-instance-type"{
	default = "r5.large"
}


variable "ec2-count"{
	default = "1"
}

variable "kms_key" {
     default = "arn:aws:kms:eu-west-1:528181836254:key/5ed23d24-73f8-4632-9200-13fc1c262102"
}

#data "aws_availability_zones" "azs" {}